# Instalación e Integracion de Nagios Core + PNP4Nagios + Grafana + NRPE + Alertas de alarmas por Gmail sobre Centos 7 <br/>
<img src="https://static1.tothenew.com/blog/wp-content/uploads/2016/05/Nagios-logo.jpg"> <br/>

<br/>

* ***IMPORTANTE***: Siempre es fundamental tener actualizado su S.O. Para Centos 7 : <br/>
------------------------------------------------------------------------------------------------
      yum update

## Instalación automatizada: Bundle Nagios. <br/>
------------------------------------------------------------------------------------------------
      - Nagios Core 4.4.5
      - PNP4Nagios
      - Grafana Server
      - Agente rele Gmail 
      - Nrpe   (latest version v1.1)
 
**Guia de apoyo de instalación en:** <br/>
[![Creative Commons License](https://www.freeiconspng.com/minicovers/red-youtube-logo-icon-8.png)](https://www.youtube.com/watch?v=YXR4rY4XiMc)
<br/>

* **1.- Requisitos para la instalación automatizada, es tener instalado estos dos paquetes.** <br/>
------------------------------------------------------------------------------------------------
      yum install -y git ansible 

* **2.- Clonar proyecto.** <br/>
-------------------------------------------------------------------------------------------------
      git clone https://gitlab.com/ska19/nagiosbundle.git

* **3.- Entrar al directorio GLPI.** <br/>
-------------------------------------------------------------------------------------------------
      cd nagiosbundle/

* **4.- Instalar proyecto ejecutando el siguiente comando.** <br/>
-------------------------------------------------------------------------------------------------
      ansible-playbook playbook.yml

> **Nota1:** A continuación se detallan las web que se instalan y sus credenciales por defecto. 
>> **Nagios:** http://ip_servidor/nagios <br/>
>> usuario: nagiosadmin <br/>
>> password: nagios <br/>
>> **Grafana:** http://ip_servidor:3000 <br/>
>> usuario: admin <br/>
>> password: admin <br/>
>> **PNP4Nagios:** http://ip_servidor/pnp4nagios <br/>

> **Nota2:** Es necesario establecer los correos que enviarán las alertas y el o los correos que recibirán, para ello consulte el siguiente ítem. <br/>
> *Instalación manual:* ***"Agente rele Gmail" punto 2-5 y 10-14***.
>>   - Reiniciar servicios *nagios* y *postfix*.




* **5.- Configurar Grafana *(capturas)*.** <br/>

![usuario y contraseña &#34;admin&#34;](https://gitlab.com/ska19/imagenes/-/blob/master/nagios/grafanainstalacion1.png "usuario y contraseña &#34;admin&#34;")<br/>
![Establecer nueva contraseña](https://gitlab.com/ska19/imagenes/-/blob/master/nagios/grafanainstalacion2.png "Establecer nueva contraseña")<br/>
![Add data source](https://gitlab.com/ska19/imagenes/-/blob/master/nagios/grafanainstalacion3.png "Add data source")<br/>
![Add PNP source](https://gitlab.com/ska19/imagenes/-/blob/master/nagios/grafanainstalacion4.png "Add PNP source")<br/>
![Agregar URL localhost](https://gitlab.com/ska19/imagenes/-/blob/master/nagios/grafanainstalacion5.png "Agregar URL localhost")<br/>
![Crear Dashboard](https://gitlab.com/ska19/imagenes/-/blob/master/nagios/grafanainstalacion6.png "Crear Dashboard2")<br/>
![Add Query](https://gitlab.com/ska19/imagenes/-/blob/master/nagios/grafanainstalacion7.png "Add Query")<br/>
![Seleccionar Host](https://gitlab.com/ska19/imagenes/-/blob/master/nagios/grafanainstalacion8.png "Seleccionar Host")<br/>
![Asignar nombre del servicio](https://gitlab.com/ska19/imagenes/-/blob/master/nagios/grafanainstalacion9.png "Asignar nombre del servicio")<br/>
![Asignar nombre del servidor](https://gitlab.com/ska19/imagenes/-/blob/master/nagios/grafanainstalacion10.png "Asignar nombre del servidor")<br/>
![Vista del servicio ping](https://gitlab.com/ska19/imagenes/-/blob/master/nagios/grafanainstalacion11.png "Vista del servicio ping")<br/>
![Vista de varios servicios](https://gitlab.com/ska19/imagenes/-/blob/master/nagios/grafanainstalacion12.png "Vista de varios servicios")<br/><br/><br/>


## Instalación manual: Nagios Core. <br/>

* **1.- Instalar repositorio epel.** <br/>
-------------------------------------------------------------------------------------------------
      yum install -y epel-release

* **2.- Instalación de paqueteria necesaria para Nagios Core.** <br/>
-------------------------------------------------------------------------------------------------
      yum install -y wget perl httpd php gcc glibc glibc-common gd gd-devel make net-snmp libpng-devel libjpeg-turbo-devel unzip vim

* **3.- Crear grupo nagcmd.** <br/>
-------------------------------------------------------------------------------------------------    
      groupadd nagcmd

* **4.- Crear usuario Nagios.** <br/>
-------------------------------------------------------------------------------------------------
      useradd nagios

* **5.- Asignar una contraseña al usuario Nagios.** <br/>
-------------------------------------------------------------------------------------------------
      passwd nagios

* **6.- Asignar el usuario Nagios al grupo nagcmd.** <br/>
-------------------------------------------------------------------------------------------------
      usermod -a -G nagcmd nagios

* **7.- Asignar el usuario Apache al grupo nagcmd.** <br/>
-------------------------------------------------------------------------------------------------
      usermod -a -G nagcmd apache

* **8.- Descargar Nagios Core.** <br/>
-------------------------------------------------------------------------------------------------
      wget https://assets.nagios.com/downloads/nagioscore/releases/nagios-4.4.5.tar.gz

* **9.- Descargar plugins para Nagios.** <br/>
-------------------------------------------------------------------------------------------------
      wget https://nagios-plugins.org/download/nagios-plugins-2.3.2.tar.gz

* **10.- Descomprimir archivo que contiene Nagios.** <br/>
-------------------------------------------------------------------------------------------------
      tar zxvf nagios-4.4.5.tar.gz

* **11.- Descomprimir archivo que contiene los plugins para Nagios.** <br/>
-------------------------------------------------------------------------------------------------
      tar zxvf nagios-plugins-2.3.2.tar.gz

* **12.- Entrar al directorio de Nagios.** <br/>
-------------------------------------------------------------------------------------------------
      cd nagios-4.4.5/

* **13.- Configurar Nagios ejecutando los siguientes comandos.** <br/>
---------------------------------------------------------------------
      ./configure -with-command-group=nagcmd 
      make all 
      make install 
      make install-init 
      make install-commandmode
      make install-config
      make install-webconf

* **14.- Entrar al directorio de plugins para Nagios.** <br/>
-------------------------------------------------------------------------------------------------
      cd nagios-plugins-2.3.2/

* **15.- Configurar plugins para Nagios ejecutando los siguientes comandos.** <br/>
---------------------------------------------------------------------------------
      ./configure --with-nagios-user=nagios --with-nagios-group=nagios
      make
      make install

* **16.- Crear usuario y contraseña para entrar a la web de administración Nagios Core.** <br/>
-------------------------------------------------------------------------------------------------
      htpasswd -c /usr/local/nagios/etc/htpasswd.users nagiosadmin

* **17.- Abrir puerto http para acceder a la web de administración de Nagios Core.** <br/>
-------------------------------------------------------------------------------------------------
      firewall-cmd --permanent --zone=public --add-service=http

* **18.- Cargar reglas de firewall.** <br/>
-------------------------------------------------------------------------------------------------
      firewall-cmd --reload

* **19.- Configurar servicios para que se inicialicen junto al sistema.** <br/>
---------------------------------------------------------------------------------
      systemctl enable httpd
      systemctl enable nagios

* **20.- Iniciar Servicios.** <br/>
---------------------------------------------------------------------------------
      systemctl start httpd
      systemctl start nagios
* **21.- Ingresar al servidor Nagios.** <br/>
----------------------------------------------------------------------------------

> http://ip_servidor/nagios <br/>
>> usuario: nagiosadmin <br/>
>> password: **********

## Instalación manual: PNP4Nagios. <br/>

* **1.- Instalar repositorio webtatic.** <br/> 
-------------------------------------------------------------------------------------------------
      yum install -y https://mirror.webtatic.com/yum/el7/webtatic-release.rpm

> **Nota:** Es posible que ya este instalado este repositorio.

* **2.- Instalación de paqueteria necesaria para PNP4nagios.** <br/>
-------------------------------------------------------------------------------------------------
      yum install -y php-gd perl-Time-HiRes rrdtool-perl rrdtool 

* **3.- Descargar PNP4Nagios.** <br/>
-------------------------------------------------------------------------------------------------
      wget https://downloads.sourceforge.net/project/pnp4nagios/PNP-0.6/pnp4nagios-0.6.26.tar.gz

* **4.- Descomprimir PNP4Nagios.** <br/>
-------------------------------------------------------------------------------------------------
      tar xzvf pnp4nagios-0.6.26.tar.gz

* **5.- Entrar al directorio de PNP4Nagios.** <br/>
-------------------------------------------------------------------------------------------------      
      cd pnp4nagios-0.6.26/

* **6.- Configurar PNP4Nagios ejecutando los siguientes comandos.** <br/>
-------------------------------------------------------------------------------------------------
      ./configure
      make all
      make fullinstall

* **7.- Configurar servicio para que se inicialice junto al sistema.** <br/>
--------------------------------------------------------------------------------------------------
      /sbin/chkconfig npcd on

* **8.- Reiniciar servicio http.** <br/>
--------------------------------------------------------------------------------------------------
      systemctl restart httpd

* **9.- Realizar primera prueba de instalación.** <br/>
--------------------------------------------------------------------------------------------------
> Desde el navegadoor ingresar a la siguiente url: <br/>
>> http://ip_servidor/pnp4nagios

* **10.- Estado de la configuración de PNP4Nagios.** <br/>
![Estado de instalación](https://gitlab.com/ska19/imagenes/-/blob/master/nagios/pnp4nagios1.png "Estado O.K")

* **11.- Eliminar archivo de instalación.** <br/>
-------------------------------------------------------------------------------------------------
      rm -rf /usr/local/pnp4nagios/share/install.php

* **12.- Ir a directorio de configuracion de Nagios.** <br/>
--------------------------------------------------------------------------------------------------
      cd /usr/local/nagios/etc

* **13.- Editar archivo de configuración de Nagios.** <br/>
--------------------------------------------------------------------------------------------------
      vim nagios.cfg

* **14.- Editar la variable ***process_performance_data***. Se debe cambiar de 0 a 1.** <br/>
---------------------------------------------------------------------------------------------------
      process_performance_data= 1

* **15.- Ir al final del archivo y añadir las siguientes lineas.** <br/>
---------------------------------------------------------------------------------------------------
      ### PNP4NAGIOS ###

      service_perfdata_file=/usr/local/pnp4nagios/var/service-perfdata 
      service_perfdata_file_template=DATATYPE::SERVICEPERFDATA\tTIMET::$TIMET$\tHOSTNAME::$HOSTNAME$\tSERVICEDESC::$SERVICEDESC$\tSERVICEPERFDATA::$SERVICEPERFDATA$\tSERVICECHECKCOMMAND::$SERVICECHECKCOMMAND$\tHOSTSTATE::$HOSTSTATE$\tHOSTSTATETYPE::$HOSTSTATETYPE$\tSERVICESTATE::$SERVICESTATE$\tSERVICESTATETYPE::$SERVICESTATETYPE$
      service_perfdata_file_mode=a 
      service_perfdata_file_processing_interval=15 
      service_perfdata_file_processing_command=process-service-perfdata-file 

      host_perfdata_file=/usr/local/pnp4nagios/var/host-perfdata 
      host_perfdata_file_template=DATATYPE::HOSTPERFDATA\tTIMET::$TIMET$\tHOSTNAME::$HOSTNAME$\tHOSTPERFDATA::$HOSTPERFDATA$\tHOSTCHECKCOMMAND::$HOSTCHECKCOMMAND$\tHOSTSTATE::$HOSTSTATE$\tHOSTSTATETYPE::$HOSTSTATETYPE$
      host_perfdata_file_mode=a 
      host_perfdata_file_processing_interval=15 
      host_perfdata_file_processing_command=process-host-perfdata-file

* **16.- Ir al directorio object de Nagios.** <br/>
---------------------------------------------------------------------------------------------------
      cd /usr/local/nagios/etc/objects

* **17.- Editar archivo ***commands.***** <br/>
---------------------------------------------------------------------------------------------------
      vim commands.cfg

* **18.- Ir al final del archivo y añadir las siguientes lineas.** <br/>
---------------------------------------------------------------------------------------------------
      # NPCD mode #

      define command {
             command_name    process-service-perfdata-file
             command_line    /bin/mv /usr/local/pnp4nagios/var/service-perfdata /usr/local/pnp4nagios/var/spool/service-perfdata.$TIMET$
             }


      define command {
             command_name    process-host-perfdata-file
             command_line    /bin/mv /usr/local/pnp4nagios/var/host-perfdata /usr/local/pnp4nagios/var/spool/host-perfdata.$TIMET$
             }
             
* **19.- En el mismo directorio de trabajo, editar el archivo ***template.cfg.***** <br/>
---------------------------------------------------------------------------------------------------
      vim templates.cfg

* **20.- Ir al final del archivo y añadir las siguientes lineas.** <br/>
---------------------------------------------------------------------------------------------------
      # PNP4NAGIOS #

      define host { 
      name          host-pnp
      action_url    /pnp4nagios/index.php/graph?host=$HOSTNAME$&srv=_HOST_' class='tips' rel='/pnp4nagios/index.php/popup?host=$HOSTNAME$&srv=_HOST 
      register      0
      }


      define service {
      name          srv-pnp
      action_url    /pnp4nagios/index.php/graph?host=$HOSTNAME$&srv=$SERVICEDESC$' class='tips' rel='/pnp4nagios/index.php/popup?host=$HOSTNAME$&srv=$SERVICEDESC$ 
      register      0
      }
      
* **21.- En el mismo directorio de trabajo, editar archivo ***localhost.cfg***.** <br/>
---------------------------------------------------------------------------------------------------
      vim localhost.cfg

* **22.- Editar host y las variables que definen los servicios.** <br/>
---------------------------------------------------------------------------------------------------
      # Define a host for the local machine

      define host {

          use                     linux-server,host-pnp          ; Name of host template to use
                                                    ; This host definition will inherit all variables that are defined
                                                    ; in (or inherited by) the linux-server host template definition.
          host_name               localhost
          alias                   localhost
          address                 127.0.0.1
          icon_image                      linux40.gif
          statusmap_image                 linux40.gd2
      } 

>> **Nota:** En este punto se añade en la variable ***use*** el host de pnp ***host-pnp*** de esta forma Nagios interpreta la grafica de este host. <br/>
>> **Nota2:** Es opcional establecer las variables ***icon_image*** y ***statusmap_image*** para establecer un icono del S.O.

      # Define a service to "ping" the local machine

      define service {

          use                     local-service,srv-pnp           ; Name of service template to use
          host_name               localhost
          service_description     PING
          check_command           check_ping!100.0,20%!500.0,60%
      }

>> **Nota:** Como se observa, se añade en la variable ***use*** el servidor de pnp ***srv-pnp*** , de esta forma Nagios generara la grafica para este servicio. <br/>
>> **Nota2:** Es necesario establecer en cada uno de los servicios establecidos en el archivo ***localhost.cfg*** (***Root Partition,Current Users,Total Processes,etc***) la variable ***srv-pnp*** para generar graficos. <br/>

* **23.- Crear archivo de configuración de PNP4Nagios para generar vista previa de los graficos.** <br/>
---------------------------------------------------------------------------------------------------
      vim /usr/local/nagios/share/ssi/status-header.ssi

* **24.- Añadir las siguientes lineas al archivo.** <br/>
---------------------------------------------------------------------------------------------------
      <script src="/pnp4nagios/media/js/jquery-min.js" type="text/javascript"></script>
      <script src="/pnp4nagios/media/js/jquery.cluetip.js" type="text/javascript"></script>
      <script type="text/javascript">
      jQuery.noConflict();
      jQuery(document).ready(function() {
      jQuery('a.tips').cluetip({ajaxCache: false, dropShadow: false,showTitle: false });
      });
      </script


* **25.- Reiniciar servicios.** <br/>
---------------------------------------------------------------------------------------------------
      systemctl restart nagios
      systemctl restart httpd
      systemctl restart npcd

## Instalación manual: Grafana Server. <br/>

* **1.- Descargar RPM para instalar Grafana.** <br/>
---------------------------------------------------------------------------------------------------
      wget https://dl.grafana.com/oss/release/grafana-6.4.3-1.x86_64.rpm

* **2.- Instalar dependencia necesaria para Grafana.** <br/>
---------------------------------------------------------------------------------------------------
      yum install -y urw-fonts

* **3.- Instalar RPM de grafana.** <br/>
---------------------------------------------------------------------------------------------------
      rpm -ivh grafana-6.4.3-1.x86_64.rpm

* **4.- Instalar dependencias de PNP4Nagios para Grafana ejecutando el siguiente comando.** <br/>
---------------------------------------------------------------------------------------------------
      grafana-cli plugins install sni-pnp-datasource

* **5.- Descargar API de Grafana.** <br/>
---------------------------------------------------------------------------------------------------
      wget https://github.com/lingej/pnp-metrics-api/raw/master/application/controller/api.php

* **6.- Mover el archivo ***api.php*** a la ruta de grafana.** <br/>
---------------------------------------------------------------------------------------------------
      mv api.php /usr/local/pnp4nagios/share/application/controllers/

* **7.- Abrir puerto 3000 tcp en firewall para Grafana.** <br/>
---------------------------------------------------------------------------------------------------
      firewall-cmd --permanent --zone=public --add-port=3000/tcp
      firewall-cmd --reload

* **8.- Editar archivo de PNP4Nagios para conceder permiso localhost al sitio PNP4Nagios.** <br/>
---------------------------------------------------------------------------------------------------
      vim /etc/httpd/conf.d/pnp4nagios.conf

* **9.- Añadir la siguiente linea.** <br/>
----------------------------------------------------------------------------------------------------
**Require ip 127.0.0.1 ::1**

      AuthName "Nagios Access"
   	  AuthType Basic
   	  AuthUserFile /usr/local/nagios/etc/htpasswd.users
   	  Require valid-user
      Require ip 127.0.0.1 ::1
      
>> **Nota:** Añadir la IP ***127.0.0.1*** ***(localhost)*** por debajo de la sentencia ***Require valid-user***

* **10.- Configurar servicio para que se inicialice junto al sistema.** <br/>
----------------------------------------------------------------------------------------------------
      systemctl enable grafana-server

* **11.- Iniciar Servicio de Grafana.** <br/>
----------------------------------------------------------------------------------------------------
      systemctl start grafana-server

* **12.- Reiniciar Servicio de http.** <br/>
----------------------------------------------------------------------------------------------------
      systemctl restart httpd

* **13.- Ingresar a la web de  grafana para su instalación.** <br/>
> http://ip_servidor:3000 <br/>
>> usuario: admin <br/>
>> password: admin

* **14.- Capturas de instalación.** <br/>
 
![usuario y contraseña &#34;admin&#34;](https://gitlab.com/ska19/imagenes/-/blob/master/nagios/grafanainstalacion1.png "usuario y contraseña &#34;admin&#34;")<br/>
![Establecer nueva contraseña](https://gitlab.com/ska19/imagenes/-/blob/master/nagios/grafanainstalacion2.png "Establecer nueva contraseña")<br/>
![Add data source](https://gitlab.com/ska19/imagenes/-/blob/master/nagios/grafanainstalacion3.png "Add data source")<br/>
![Add PNP source](https://gitlab.com/ska19/imagenes/-/blob/master/nagios/grafanainstalacion4.png "Add PNP source")<br/>
![Agregar URL localhost](https://gitlab.com/ska19/imagenes/-/blob/master/nagios/grafanainstalacion5.png "Agregar URL localhost")<br/>
![Crear Dashboard](https://gitlab.com/ska19/imagenes/-/blob/master/nagios/grafanainstalacion6.png "Crear Dashboard2")<br/>
![Add Query](https://gitlab.com/ska19/imagenes/-/blob/master/nagios/grafanainstalacion7.png "Add Query")<br/>
![Seleccionar Host](https://gitlab.com/ska19/imagenes/-/blob/master/nagios/grafanainstalacion8.png "Seleccionar Host")<br/>
![Asignar nombre del servicio](https://gitlab.com/ska19/imagenes/-/blob/master/nagios/grafanainstalacion9.png "Asignar nombre del servicio")<br/>
![Asignar nombre del servidor](https://gitlab.com/ska19/imagenes/-/blob/master/nagios/grafanainstalacion10.png "Asignar nombre del servidor")<br/>
![Vista del servicio ping](https://gitlab.com/ska19/imagenes/-/blob/master/nagios/grafanainstalacion11.png "Vista del servicio ping")<br/>
![Vista de varios servicios](https://gitlab.com/ska19/imagenes/-/blob/master/nagios/grafanainstalacion12.png "Vista de varios servicios")<br/>

## Instalación manual: Agente rele Gmail. <br/>

* **1.- Instalación de paqueteria necesaria para agente gmail.** <br/>
----------------------------------------------------------------------------------------------------
      yum install -y postfix mailx cyrus-sasl cyrus-sasl-plain

* **2.- Entrar al directorio de configuración de postfix.** <br/>
----------------------------------------------------------------------------------------------------
      cd /etc/postfix

* **3.- Crear archivo para autentificar el correo emisor de alertas.** <br/>
----------------------------------------------------------------------------------------------------
      vim sasl_passwd

* **4.- Añadir las siguientes lineas.** <br/>
----------------------------------------------------------------------------------------------------
      [smtp.gmail.com]:587 correoEmisor@gmail.com:contraseña_correo
      
> **Nota:** Este correo sera el que envie las alertas a los correos destinatarios. <br/>

> **¡IMPORTANTE!:** Para que Nagios envie las alarmas, la cuenta de **Gmail** debe permitir que las aplicaciones menos seguras accedan a tu cuenta. <br/>
>                Para ello visitar:
>>  https://myaccount.google.com/lesssecureapps

* **5.- En el mismo directorio de trabajo, crear la tabla de postfix del fichero creado con el siguiente comando.** <br/>
----------------------------------------------------------------------------------------------------
      postmap /etc/postfix/sasl_passwd

* **6.- En el mismo directorio de trabajo, editar el archivo *master.cf*** <br/>
----------------------------------------------------------------------------------------------------
      vim master.cf

> **Editar la siguiente linea:** <br/>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;smtp          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;inet        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;n               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;n       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;smtpd <br/>

>  **Por la siguiente linea:**  <br/>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;587          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;inet        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;n               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;n       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;smtpd <br/>




* **7.- En el mismo directorio de trabajo, editar el archivo main.cf** <br/>
----------------------------------------------------------------------------------------------------
      vim main.cf

* **8.- Ir al final del archivo y añadir las siguientes lineas.** <br/>
----------------------------------------------------------------------------------------------------
      relayhost = [smtp.gmail.com]:587
      smtp_use_tls=yes
      smtp_sasl_auth_enable = yes
      smtp_sasl_password_maps = hash:/etc/postfix/sasl_passwd
      smtp_sasl_security_options = noanonymous
      smtp_sasl_mechanism_filter = plain
      smtp_tls_CAfile = /etc/ssl/certs/ca-bundle.crt

* **9.- Enviar correo de pruebas con el siguiene comando.** <br/>
----------------------------------------------------------------------------------------------------
      mail -s "Asunto del mensaje" correoReceptor@gmail.com

>  **Modo de uso:**  
>>        -   Una vez completado los campos "asunto del mensaje" y "correoReceptor" presionar la tecla ENTER.
>>        -   Escribir el cuerpo del mensaje, para finalizar presione ENTER
>>        -   Para enviar correo presionar CTRL + D


* **10.- Entrar al directorio de configuración de Nagios.** <br/>
----------------------------------------------------------------------------------------------------
      cd /usr/local/nagios/etc/objects/

* **11.- Editar archivo de configuración *contacts.cfg*** <br/>
----------------------------------------------------------------------------------------------------
      vim contacts.cfg

* **12.- Establecer correo que enviara las notificaciones.** <br/>
----------------------------------------------------------------------------------------------------      
      define contact {

            contact_name            		nagiosadmin             ; Short name of user
            use                     		generic-contact         ; Inherit default values from generic-contact template (defined above)
            alias                   		Nagios Admin            ; Full name of user
            email                   		correoReceptor@gmail.com ; <<***** CHANGE THIS TO YOUR EMAIL ADDRESS ******
            service_notification_commands 	        notify-service-by-email
            host_notification_commands 		notify-host-by-email
            }

> **Nota1:** Se debe cambiar en la variable ***email*** el correo por defecto y establecer el correo al que le llegaran las alarmas. <br/>
> **Nota2:** Las variables *service* y *host* *notification* *commands* son las encargadas de notificar vía correo las alarmas.


* **13.- Configurar servicio para que se inicialice junto al sistema.** <br/>
----------------------------------------------------------------------------------------------------
      systemctl enable postfix

* **14.- Reiniciar servicios.** <br/>
----------------------------------------------------------------------------------------------------
      systemctl restart nagios
      systemctl restart postfix


-------------
## Instalación manual:  NRPE.

* **1.- Instalación de paqueteria necesaria para NRPE.** <br/>
-------------------------------------------------------------------------------------------------
      yum install -y glibc openssl openssl-devel

* **2.- Descargar nrpe desde la fuente..** <br/>
-------------------------------------------------------------------------------------------------
      wget https://github.com/NagiosEnterprises/nrpe/archive/nrpe-3.2.1.tar.gz

* **3.- Descomprimir archivo que contiene nrpe.** <br/>
-------------------------------------------------------------------------------------------------
      tar xzvf nrpe-3.2.1.tar.gz

* **4.- Entrar al directorio de nrpe.** <br/>
-------------------------------------------------------------------------------------------------
      cd nrpe-nrpe-3.2.1/

* **5.- Compilar nrpe ejecutando los siguientes comandos.** <br/>
---------------------------------------------------------------------
      ./configure --enable-command-args
      make all
      make install-groups-users
      make install
      make install-config
      make install-init

**Donde**

| Comando | Comentario |
| ------ | ------ |
| ./configure --enable-command-args | Si desea pasar argumentos a través de NRPE, debe especificar esto en la opción de configuración como se indica ***--enable-command-args***. Puede omitir esta opción (La eliminación de este indicador requerirá que todos los argumentos se establezcan explícitamente en el archivo nrpe.cfg en cada servidor monitoreado).|
| make all | Tomar argumentos.   |
|make install-groups-users | Crear usuario y grupo Nagios. |
|make install | Instala los archivos binarios, el demonio ***NRPE*** y el complemento ***check_nrpe***. Si solo desea instalar el demonio, ejecute el comando: ***install-daemon*** (Es útil tener el complemento ***check_nrpe*** instalado para fines de prueba). |
|make install-config | Instalar archivos de configuración. |
|make install-init | Instalar archivos del servicio o demonio. |

* **6.- Habilitar para que el servicio nrpe inicialice junto al sistema.** <br/>
---------------------------------------------------------------------------------
      systemctl enable nrpe

* **7.- Iniciar Servicio nrpe.** <br/>
---------------------------------------------------------------------------------
      systemctl start nrpe

* **8.-  Entrar al directorio /etc/.** <br/>
-------------------------------------------------------------------------------------------------
      cd /etc/

* **9.-  Editar archivo services.** <br/>
-------------------------------------------------------------------------------------------------
      vim services

* **10.- Añadir la siguiente linea al final del archivo.** <br/>
-------------------------------------------------------------------------------------------------
     nrpe   		 5666/tcp		# Nagios Services

> **Nota:** Este archivo es utilizado por los servidores y por los clientes para obtener el número de puerto en el que deben escuchar o al que deben enviar peticione, de forma que se pueda cambiar (aunque no es lo habitual) un número de puerto sin afectar a las aplicaciones.

* **11.- Abrir puerto nrpe en firewall.** <br/>
-------------------------------------------------------------------------------------------------
      firewall-cmd --permanent --zone=public --add-port=5666/tcp

> **NOTA:** Es probable que deba abrir también el puerto ***12489/tcp*** para que los clientes windows puedan comunicarse con el servidor nrpe.
>> firewall-cmd --permanent --zone=public --add-port=12489/tcp <br/>
>> firewall-cmd --reload


* **12.- Cargar reglas de firewall.** <br/>
-------------------------------------------------------------------------------------------------
      firewall-cmd --reload

* **13.- Entrar al directorio etc de Nagios.** <br/>
---------------------------------------------------------------------------------
      cd /usr/local/nagios/etc/

* **14.- Editar archivo de congiuracion de nrpe.** <br/>
-------------------------------------------------------------------------------------------------
      vim nrpe.cfg

**Editar:**

| Línea original| Línea nueva	| Comentario	|
| ------       | ------         | ------        |        
| allowed_hosts=127.0.0.1,::1         | allowed_hosts=127.0.0.1,::1,***192.168.100.0/24***          |   Deben establecer su propia IP de Red de la LAN más la mascara de red o bien solo la IP del servidor NRPE.             |         
| dont_blame_nrpe=0        | dont_blame_nrpe=***1***           | Determina si el demonio NRPE permitirá o no que los clientes especifiquen argumentos para los comandos que se ejecutan, permitiendo configuraziones mas avanzadas.              |


* **15.- Reiniciar servicio Nagios para que tome los cambios del archivo nrpg.cfg** <br/>
-------------------------------------------------------------------------------------------------
     systemctl restart nagios

* **16.- Testear NRPE en servidor Nagios.** <br/>
-------------------------------------------------------------------------------------------------
     check_nrpe -H IP_Servidor

> ***NRPE v3.2.1***


---------------------
*Dudas o consultas:* <br/>
***aa.valdivialopez@gmail.com***
